/*
* Main API File.
*/

//Dependencies
const http = require('http');
const https = require('https');
const fs = require('fs');
//add config file
config = require('./config');
serverutils = require('./serverutils');


//***************HTTP****************
const httpServer = http.createServer(function(req, res){
  serverutils.unifiedServer(req,res, handlers, router);

})//end httpServer

httpServer.listen(config.httpPort, function(){
  console.log('The server is running on port: ' +config.httpPort + ' in ' +config.envName+ ' mode')

})

//***************HTTPS******************
var httpsServerOptions = {
  //The key and the certificate are what actually make encryption and decryption happen
  'key': fs.readFileSync('./https/key.pem'),
  'cert':fs.readFileSync('./https/cert.pem')
};

const httpsServer = https.createServer(httpsServerOptions,function(req, res){
  serverutils.unifiedServer(req,res,handlers, router);
})//end httpServer

httpsServer.listen(config.httpsPort, function(){
  console.log('The server is running on port: ' +config.httpsPort + ' in ' +config.envName+ ' mode')
})

//******************HANDLERS*************
// Define all the handlers
var handlers = {};

// Ping handler
handlers.ping = function(data,callback){
  callback(200,{'name': 'remigio'});
  console.log(data);
};

handlers.hello = function(data, callback){
  callback(200,{'msg': 'Hi, if you see this message in JSON format, the app is working' });
};

// Not found handler
handlers.notFound = function(data,callback){
  callback(404);
};

//******************ROUTERS*************
// Define the request router
var router = {
  'hello' : handlers.hello,
  'ping': handlers.ping,
};
