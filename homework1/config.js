/*
* Contains all configuration variables.
*/

let environments = {};

//Staging environment

environments.staging = {
  'httpPort' : 3000,
  'httpsPort': 3001,
  'envName'  : 'staging',
};

environments.production = {
  'httpPort' : 5000,
  'httpsPort': 5001,
  'envName'  : 'production',
};

//which environment was passed as a command line argument?
let currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '' ;

//which environment should we export?
let environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

//export the module we want to use

module.exports = environmentToExport;
