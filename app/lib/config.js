/*
* Create and export configuration variables
*/

//Container for all environments.

let environments = {};

//Staging (default environment)

environments.staging = {
  'httpPort' : 3000,
  'httpsPort' : 3001,
  'envName' : 'staging',
  'hashingSecret': 'thisIsASecret*/-',
};//end staging environment

environments.production = {
  'httpPort' : 5000,
  'httpsPort' : 5001,
  'envName' : 'production',
  'hashingSecret': 'thisIsASecret*/-',
};// end production environment

// determine which environment was passed as a command-line argument
let currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '' ;
console.log(typeof(currentEnvironment));
//Check that the currentEnvironment is one of the environments above, if not, default to Staging

let environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

// the last thing we do is just export the module we want to use.

module.exports = environmentToExport;
