/*
* Library for storing and editing data.
*/

// Dependencies
const fs = require('fs');
const path = require('path');
const helpers = require('./helpers');

//Container for the module (to be exported)

var lib = {};

//Base directory of the data folder

lib.baseDir = path.join(__dirname, '/../.data/');

// Write data to a file
lib.create = function(dir, file, data, callback){
  //open the file for writing
  fs.open(lib.baseDir + dir + '/' + file + '.json', 'wx', function(err, fileDescriptor){
    if (!err && fileDescriptor) { //a fileDescriptor is wa way to uniquely identify a specific file
      // convert the data to string
      var stringData = JSON.stringify(data);
      //write the file and close it
      fs.writeFile(fileDescriptor, stringData, function(err){
        if(!err){
          fs.close(fileDescriptor, function(error){
            if (!err) {
              callback(false); //when there is no error in closing the file, we still need to pass false to the callback.
            }else {
              callback('Error closing new file');
            }
          });
        }else {
          callback('Error writing to new file');
        }

      });

    }else {
      callback('Could not create new file, it may already exist');
    }

  });
};// end function create


//Read the data from a file

lib.read = function(dir, file, callback){
  fs.readFile(lib.baseDir+dir+'/'+file+'.json','utf8',function(err,data){
    if (!err && data) {
      var parsedData = helpers.parseJsonToObject(data);
      callback(false, parsedData);
    }else {
      callback (err, data);
    }
  });
};//end function read

//Update data inside a file:

lib.update = function(dir,file,data,callback){
  //open the file for writing:
  fs.open(lib.baseDir+dir+'/'+file+'.json','r+',function(err, fileDescriptor){
    if (!err && fileDescriptor) {
        var stringData = JSON.stringify(data);

        //truncate the content of the file
        fs.truncate(fileDescriptor, function(err){
          if (!err) {
            //write to the file and close it
            fs.writeFile(fileDescriptor, stringData, function(err){
              if(!err){
                fs.close(fileDescriptor, function(error){
                  if (!err) {
                    callback(false); //when there is no error in closing the file, we still need to pass false to the callback.
                  }else {
                    callback('Error closing new file');
                  }
                });
              }else {
                callback('Error writing to existing file');
              }

            });
          }else {
            callback('Error truncating file');
          }
        });
    }else {
      callback('Could not open the file for updating, it may not exist yet');
    }
  });
};//end update

//Delete a file:
lib.delete = function(dir,file,callback){
  // Unlink (remove from the filesystem)
  fs.unlink(lib.baseDir+dir+'/'+file+'.json', function(err){
    if (!err) {
      callback(false);
      }else {
        callback('Error deleting the file');
    }
  });
};//end delete


//Export it

module.exports = lib;
