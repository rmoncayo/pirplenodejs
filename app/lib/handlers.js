/*
* Request handlers
*/
//Dependencies:

var _data = require('./data');
var helpers = require('./helpers');


// Define all the handlers
var handlers = {};

//users handlers
handlers.users = function(data, callback){
  var acceptableMethods = ['post', 'get', 'put', 'delete'];
  if (acceptableMethods.indexOf(data.method) > -1) {
    handlers._users[data.method](data,callback); //we are calling a "private" method for the user handler
  }else {
    callback(405); //this is the HTTP return code for method not allowed
  }
}

//Container for the users submethods:
handlers._users ={};
//users - post
//requiered data: firstName, lastName, phone, password, tosAgreement
handlers._users.post = function(data,callback){
  //check that all requiered fields are filled out
  //Use postman with a post request with the following payload:
//  {	"firstName":"Remigio","lastName":"Moncayo","phone": "0123456489","password":"MyPassword01","tosAgreement":true}

  var firstName = typeof(data.payload.firstName) == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
  var lastName = typeof(data.payload.lastName) == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
  var phone = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false;
  var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
  var tosAgreement = typeof(data.payload.tosAgreement) == 'boolean' && data.payload.tosAgreement == true ? true : false;

  if (firstName && lastName && phone && password && tosAgreement) {
    //Make sure that the user doesn't already exist.
    _data.read('users', phone, function(err, data){
      if (err) {

        //if we can't find the phone in our data, we're good to go.
        //We need to hash the password before saving it
        var hashedPassword = helpers.hash(password);

        if (hashedPassword) {
          //Create the user object
          var userObject = {
            'firstName' : firstName,
            'lastName': lastName,
            'phone' : phone,
            'hashedPassword' : hashedPassword,
            'tosAgreement' : true,
          };

          //persist the user to disk (store it)

          _data.create('users',phone,userObject, function(err){
            if (!err) {
              callback(200);
            }else {
              console.log(err);
              callback(500, {'Error': 'Could not create the new user'});
            }
          });
        }else {
          callback(500, {'Error': 'Could not hash the User\'s password'})
        }

      }else {
        //User already exists
        callback(400, {'Error': 'A user with that phone number already exists'});
      }
    });
  }else {
    callback(400,{'Error' : 'Missing required fields'});
  }

};

//users - get
//Required data: phone
//OPtional data: none.
//Send the request as localhost:3000/users?phone=0123456789
// @TODO only an authenticated user access their object. Don't let them access anyone else's data
handlers._users.get = function(data,callback){
 //check that the phone number provided is valid. It will come in the queryString
  var phone  = typeof(data.queryStringObject.phone) == 'string' && data.queryStringObject.phone.trim().length == 10 ? data.queryStringObject.phone.trim() : false;

  if (phone) {
    _data.read('users', phone, function(err,data){
      if (!err && data) {
        // Remove the hashed password from the user object before returning it to the requester
        delete data.hashedPassword;
        callback(200,data); //this is the data coming back from the read function
      }else {
        callback(404)
      }
    });
  }else {
    callback(400, {'Error': 'Missing required field'});
  }
};

//users - put
//Required data: phone
//Optional data: firstName, lastName, password
//At least one of the optional parameters must be specified
// @TODO only an authenticated user access their object. Don't let them access anyone else's data

handlers._users.put = function(data,callback){
  var phone  = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false;

  // check for the optional fields

  var firstName = typeof(data.payload.firstName) == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
  var lastName = typeof(data.payload.lastName) == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
  var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;

  //Error if the phone is invalid

  if (phone) {
    //Error if nothing is sent to update
    if (firstName || lastName || password) {
      //lookup the users to check if our user exists:
      _data.read ('users', phone, function(err,userData){
        if (!err && userData) {
          //Update the fields necessary
          if (firstName) {
            userData.firstName = firstName;
          }
          if (lastName) {
            userData.lastName = lastName;
          }
          if (password) {
            userData.hashedPassword = helpers.hash(password);
          }

          //Store the new updates
          _data.update('users', phone, userData, function(err){
            if (!err) {
              callback(200);
            }else {
              console.log(err);
              callback(500, {'Error': 'Internal error. Could not update'});
            }
          });

        }else {
          callback(400, {'Error' : 'The specified user does not exist'});
        }
      });
    }else {
    callback(400, {'Error': 'Missing fields to update'});
    }
  }else {
    callback(400, {'Error': 'Missing required field'});
  }
};

//users - post
//required field: phone
// @TODO only an authenticated user access their object. Don't let them access anyone else's data
// @TODO Cleanup(delete) any other data files associated with this user
handlers._users.delete = function(data,callback){
 //Check that the phone number is valid
 //check that the phone number provided is valid. It will come in the queryString
  //send a delete request in postman as: localhost:3000/users?phone=1123456489
  var phone  = typeof(data.queryStringObject.phone) == 'string' && data.queryStringObject.phone.trim().length == 10 ? data.queryStringObject.phone.trim() : false;

  if (phone) {
    _data.read('users', phone, function(err,data){
      if (!err && data) {
      _data.delete('users', phone, function(err){
        if (!err) {
          callback(200);
        }else {
          callback(500, {'Error': 'Could not delete the specified user'});
        }
      });
      }else {
        callback(404, {'Error': 'Could not find the specified user'});
      }
    });
  }else {
    callback(400, {'Error': 'Missing required field'});
  }

};


// Ping handler
handlers.ping = function(data,callback){
    callback(200,{'name': 'remigio'});
};

// Sample handler
handlers.sample = function(data,callback){
    callback(406,{'name':'sample handler'});
};

// Not found handler
handlers.notFound = function(data,callback){
  callback(404);
};


module.exports = handlers;
