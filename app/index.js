/*
*
* Node course. Primary file for the api
*
*/

// Dependencies
var http = require('http');
var https = require('https');
var url = require('url');
var StringDecoder = require('string_decoder').StringDecoder;
var config = require("./lib/config");
var fs = require('fs');
var _data = require('./lib/data');
var handlers = require('./lib/handlers');
var helpers = require('./lib/helpers');
//TESTING WRITING FILES
//@TODO  delete this


/*_data.create('test', 'NewFile', {'foo' : 'bar'}, function(err){
  console.log('This was the error: ', err);
});*/


//testing reading files:
_data.read('test', 'NewFile', function(err,data){
  console.log('This was the error: ', err);
  console.log('\nThis is the data: ', data);
});

/*console.log('\n Now testing updating file: ');

_data.update('test', 'NewFile',{'fizz': 'buzz'}, function(err){
  console.log('This was the error: ', err);
});*/

//Deleting the file:

//_data.delete('test', 'NewFile', function(err){
//  console.log ('This was the error: ', err);
//});


//To start the server in production mode: NODE_ENV=production node index.js
//****************************HTTP**********************
 // Instantiate the http server
var httpServer = http.createServer(function(req,res){
  unifiedServer(req,res);

});//end createServer

// Start the server
httpServer.listen(config.httpPort,function(){
  console.log('The server is up and running on port '+config.httpPort+' in '+config.envName+' mode');
});
//**************************HTTPS**********************
var httpsServerOptions = {
  //The key and the certificate are what actually make encryption and decryption happen
  'key': fs.readFileSync('./https/key.pem'),
  'cert':fs.readFileSync('./https/cert.pem')
};
//instantiate the https server
var httpsServer = https.createServer(httpsServerOptions,function(req,res){
  unifiedServer(req,res);

});//end createServer

//start the https server

httpsServer.listen(config.httpsPort,function(){
  console.log('The server is up and running on port '+config.httpsPort+' in '+config.envName+' mode');
});


// 09.https. All the server logic for both, the HTTP and the HTTPS servers.catch
// Copy what we had before
var unifiedServer = function(req,res){

  //
  // Parse the url
  var parsedUrl = url.parse(req.url, true);

  // Get the path
  var path = parsedUrl.pathname;
  var trimmedPath = path.replace(/^\/+|\/+$/g, '');
  console.log(trimmedPath);

  // Get the query string as an object
  var queryStringObject = parsedUrl.query;

  // Get the HTTP method
  var method = req.method.toLowerCase();

  //Get the headers as an object
  var headers = req.headers;

  // Get the payload,if any
  var decoder = new StringDecoder('utf-8');
  var buffer = '';
  req.on('data', function(data) {
      buffer += decoder.write(data);
  });
  req.on('end', function() {
      buffer += decoder.end();

      // Check the router for a matching path for a handler. If one is not found, use the notFound handler instead.
      var chosenHandler = typeof(router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handlers.notFound;

      // Construct the data object to send to the handler
      var data = {
        'trimmedPath' : trimmedPath,
        'queryStringObject' : queryStringObject,
        'method' : method,
        'headers' : headers,
        'payload' : helpers.parseJsonToObject(buffer),
      };

      // Route the request to the handler specified in the router
      chosenHandler(data,function(statusCode,payload){

        // Use the status code returned from the handler, or set the default status code to 200
        statusCode = typeof(statusCode) === 'number' ? statusCode : 200;

        // Use the payload returned from the handler, or set the default payload to an empty object
        payload = typeof(payload) === 'object'? payload : {};

        // Convert the payload to a string
        var payloadString = JSON.stringify(payload);

        // Return the response
        //Now we inform the client that we're sending JSON
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(statusCode);
        res.end(payloadString);
        console.log("Returning this response: ",statusCode,payloadString);

      }); //end chosenHandler function

  }); //end req.on.end

};

// Define the request router
var router = {
  'sample' : handlers.sample,
  'ping': handlers.ping,
  'users' : handlers.users,
};
