/*
* Create and export configuration variables
*/

//Container for all environments.

let environments = {};

//Staging (default environment)

environments.staging = {
  'port' : 3000,
  'envName' : 'staging'
};//end staging environment

environments.production = {
  'port' : 5000,
  'envName' : 'production'
};// end production environment

// determine which environment was passed as a command-line argument
let currentEnvironment = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '' ;

//Check that the currentEnvironment is one of the environments above, if not, default to Staging

let environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

// the last thing we do is just export the module we want to use.

module.exports = environmentToExport;

//Start the server as: NODE_ENV=production node index.js
