/*
*
* Node course. Primary file for the api
*
*/

//Dependencies
const http = require('http');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
//respond to all requests with a string
const server = http.createServer(function(req, res){

    //Get the URL and parse it
    //the req object has a lot of info. We now need only the url.
    const parsedUrl = url.parse(req.url, true);

    //Get the path from the the parsed URL object.
    const path = parsedUrl.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g,'');

    //04. Get the query string as an object
    //curl localhost:3000/foo/bar?fizz=buzz
    const queryStringObject = parsedUrl.query;

    //03.Parsing HTTP Methods: Get the http method:
    //The method is one of the objects available in req
    const method = req.method.toLowerCase();

    //05.Parsing Headers. Get the headers as an object
    const headers = req.headers;

    //06. Get the payload (We need to change the method to POST(raw))
    const decoder = new StringDecoder('utf-8')
    //We catch the string stream in a buffer until the stream finishes.
    let buffer = '';
    req.on('data', function(data){
      buffer += decoder.write(data);
    });
    req.on('end', function(){
      buffer += decoder.end();
        res.end('Hello world\nRequest recieved on path ' + trimmedPath + 'with method '+ method + '\n' );
        console.log('Request received with this payload:', buffer);
      });


});
//Start the server listen on p. 3000
server.listen(3000, function(){
    console.log("the server is listening on port 3000");
});
