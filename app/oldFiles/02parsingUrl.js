/*
*
* Node course. Primary file for the api
*
*/

//Dependencies
const http = require('http');
const url = require('url');
//respond to all requests with a string
const server = http.createServer(function(req, res){
    
    //Get the URL and parse it
    //the req object has a lot of info. We now need only the url.
    const parsedUrl = url.parse(req.url, true);
    //Get the path from the the parsed URL object.
    const path = parsedUrl.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g,'');
    
    //Send the response
    res.end('Hello world\nRequest recieved on path ' + trimmedPath +'\n' );
    //Log the request path.
    console.log('Request received on  path '+ trimmedPath);
    
});
//Start the server listen on p. 3000
server.listen(3000, function(){
    console.log("the server is listening on port 3000");
});
