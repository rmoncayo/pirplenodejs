/*
*
* Node course. Primary file for the api
*
*/

//Dependencies
const http = require('http');
const url = require('url');
//respond to all requests with a string
const server = http.createServer(function(req, res){
    
    //Get the URL and parse it
    //the req object has a lot of info. We now need only the url.
    const parsedUrl = url.parse(req.url, true);
    
    //Get the path from the the parsed URL object.
    const path = parsedUrl.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g,'');

    //04. Get the query string as an object
    //curl localhost:3000/foo/bar?fizz=buzz
    const queryStringObject = parsedUrl.query;

    //03.Parsing HTTP Methods: Get the http method:
    //The method is one of the objects available in req
    const method = req.method.toLowerCase();

    //05.Parsing Headers. Get the headers as an object
    const headers = req.headers;
    
    //Send the response
    res.end('Hello world\nRequest recieved on path ' + trimmedPath + 'with method '+ method +'\n' );
    
    //Log the request path.
    console.log('Request received on  path '+ trimmedPath);
    console.log('with method ' + method);
    console.log('and with these query string parameters',queryStringObject);
    console.log('request received with these headers: ', headers);
    
});
//Start the server listen on p. 3000
server.listen(3000, function(){
    console.log("the server is listening on port 3000");
});
