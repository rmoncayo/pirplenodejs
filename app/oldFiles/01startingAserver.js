/*
*
* Node course. Primary file for the api
*
*/

//Dependencies
const http = require('http');
//respond to all requests with a string

const server = http.createServer(function(req, res){
    res.end('Hello world\n');
});
//Start the server listen on p. 3000
server.listen(3000, function(){
    console.log("the server is listening on port 3000");
});
